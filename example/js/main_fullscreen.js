function updateData(currentLocation, radius) {
	$('#latitude').html(currentLocation.latitude.toFixed(8));
	$('#longitude').html(currentLocation.longitude.toFixed(8));
	$('#radiusInput').val(radius);
}

$('#radiusInput').change(function(e) {
	$('#map-container').locationpicker('radius', e.target.value);
});

$('#map-container').locationpicker({
	radius: 150,
	draggable: true,
	clickable: true,
	circleOptions: {
		editable: true,
		strokeColor: "orange",
		fillColor: "orange"
	},
	afterLoad: function(currentLocation, radius) {
		updateData(currentLocation, radius);
	},
	afterChange: function(currentLocation, radius) {
		updateData(currentLocation, radius);
	}
});