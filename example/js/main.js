function updateData(currentLocation, radius) {
	$('.radiusSpan').html(radius);
	$('.latitudeSpan').html(currentLocation.latitude.toFixed(8));
	$('.longitudeSpan').html(currentLocation.longitude.toFixed(8));
	$('#latitude').val(currentLocation.latitude.toFixed(8));
	$('#longitude').val(currentLocation.longitude.toFixed(8));
	$('#radius').val(radius);
	console.log("Updating location..");
	console.log(currentLocation);
	console.log("Radius: " + radius);
}

$('#radiusInput').change(function(e) {
	$('#map-container').locationpicker('radius', e.target.value);
});

$('#mapModal').on('shown.bs.modal', function () {
	$('#map-container').locationpicker({
		radius: 150,
		minRadius: 900,
		maxRadius: 1000,
		draggable: true,
		clickable: true,
		circleOptions: {
			editable: true,
			strokeColor: "orange",
			strokeWeight: 1.5,
			fillColor: "orange",
			fillOpacity: 0.2
		},
		afterLoad: function(currentLocation, radius) {
			updateData(currentLocation, radius);
		},
		afterChange: function(currentLocation, radius) {
			updateData(currentLocation, radius);
		}
	});
  $('#map-container').locationpicker('autosize');
  $('#address').focus();
});

$('#address').keypress(function(e) {
	var code = e.keyCode || e.which;
	if (code == 13) {
		// Enter pressed
		$('#map-container').locationpicker('address', $('#address').val(), function(e) {
			if (e.noResults) {
				console.log(e);
				alert("Fant ingen steder med dette navnet");
			}
		});
		$('#address').select();
	}
});

$('#modalSaveButton').click(function() {
	$('#coordinatesText').html($('#latitude').val() + ', ' + $('#longitude').val());
	$('#radiusInput').val($('#radius').val());
});

$('#mapModal').on('hide.bs.modal', function () {
	$('#map-container').locationpicker('destroy'); // Destroy the plugin
});